package usecases

import (
	"blogservice/configs"
	"blogservice/modules/entities"
	"blogservice/modules/logs"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type blogService struct {
	blogRepo entities.BlogRepository
	cfg      *configs.Configs
}

func NewUserService(blogRepo entities.BlogRepository, cfg *configs.Configs) entities.BlogService {
	return &blogService{
		blogRepo: blogRepo,
		cfg:      cfg,
	}
}

func (s *blogService) BlogCreated(userId int, blogReq *entities.BlogRequest) (int, fiber.Map) {
	check, err := s.blogRepo.CheckUser(userId)
	if err != nil {
		logs.Info("There are no users in the system.", zap.String("error", err.Error()))
		return fiber.ErrBadRequest.Code, fiber.Map{
			"message": fmt.Sprintf("BadRequest : %v", err),
		}
	}
	user, err := s.blogRepo.GetUser(check)
	if err != nil {
		logs.Info("Can't get user.", zap.String("error", err.Error()))
		return fiber.ErrBadRequest.Code, fiber.Map{
			"message": fmt.Sprintf("BadRequest : %v", err),
		}
	}
	blog := entities.Blog{
		Title:      blogReq.Title,
		BlogDesc:   blogReq.BlogDesc,
		Content:    blogReq.Content,
		CoverImage: blogReq.CoverImage,
		UserId:     user.ID,
		UserName:   user.Name,
		UserDesc:   user.Description,
		UserImage:  user.UserImage,
	}

	created, err := s.blogRepo.CreateBlog(&blog)
	if err != nil {
		logs.Info("Can't create blog", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"message": fmt.Sprintf("BadRequest : %v", err),
		}
	}

	return fiber.StatusOK, fiber.Map{
		"message":    "blog successfully created.",
		"data":       created,
		"status":     "OK",
		"statusCode": 200,
	}
}

func (s *blogService) BlogGets() (*[]entities.BlogRes, error) {
	blogs, err := s.blogRepo.GetBlogs()
	if err != nil {
		logs.Info("Can't get blogs", zap.String("error", err.Error()))

		return nil, err
	}
	blogsResponse := []entities.BlogRes{}
	for _, item := range blogs {
		Response := entities.BlogRes{
			BlogId:     item.ID,
			Title:      item.Title,
			BlogDesc:   item.BlogDesc,
			Content:    item.Content,
			CoverImage: item.CoverImage,
			UserName:   item.UserName,
			UserDesc:   item.UserDesc,
			UserImage:  item.UserImage,
		}
		blogsResponse = append(blogsResponse, Response)
	}
	return &blogsResponse, nil
}

func (s *blogService) BlogGetUID(userId uint) (*[]entities.BlogRes, error) {
	blog, err := s.blogRepo.GetBlogUID(userId)
	if err != nil {
		logs.Info("Can't get blog", zap.String("error", err.Error()))

		return nil, err
	}
	blogsResponse := []entities.BlogRes{}
	for _, item := range *blog {
		Response := entities.BlogRes{
			BlogId:     item.ID,
			Title:      item.Title,
			BlogDesc:   item.BlogDesc,
			Content:    item.Content,
			CoverImage: item.CoverImage,
			UserName:   item.UserName,
			UserDesc:   item.UserDesc,
			UserImage:  item.UserImage,
		}
		blogsResponse = append(blogsResponse, Response)
	}
	return &blogsResponse, nil
}

func (s *blogService) BlogDelete(blogId uint) (int, fiber.Map) {
	delete, err := s.blogRepo.DeleteBlog(blogId)
	if err != nil {
		logs.Info("Can't delete blog", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"message": fmt.Sprintf("BadRequest : %v", err),
		}
	}
	return fiber.StatusOK, fiber.Map{
		"message":    "Delete Blog Successfully.",
		"blogId":     delete,
		"status":     "OK",
		"statusCode": 200,
	}
}

func (s *blogService) CheckBlackList(token string) (int, error) {
	check, err := s.blogRepo.CheckBlackList(token)
	if err != nil {
		logs.Info("Can't check blacklist", zap.String("error", err.Error()))

		return 0, err
	}
	return check, nil
}

func (s *blogService) BlogGetBID(blogId uint) (int, fiber.Map) {
	getBlog, err := s.blogRepo.GetBlogBID(blogId)
	if err != nil {
		logs.Info("Can't get blog", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"message": fmt.Sprintf("BadRequest : %v", err),
		}
	}
	return fiber.StatusOK, fiber.Map{
		"message":    "Blog Retrieved Successfully.",
		"data":       getBlog,
		"status":     "OK",
		"statusCode": 200,
	}
}

func (s *blogService) BlogUpdate(blogId uint, req entities.BlogRequest, option entities.UpdateOption) (int, fiber.Map) {
	getBlog, err := s.blogRepo.GetBlogBID(blogId)
	if err != nil {
		logs.Error(fmt.Sprintf("Blog : [%v] does not exist.", blogId), zap.String("error", err.Error()))
		return fiber.ErrBadRequest.Code, fiber.Map{
			"error": "Failed to retrieve blog",
		}
	}

	if option.Field == "title" {
		getBlog.Title = req.Title
	} else if option.Field == "blogDesc" {
		getBlog.BlogDesc = req.BlogDesc
	} else if option.Field == "content" {
		getBlog.Content = req.Content
	} else if option.Field == "coverImage" {
		getBlog.CoverImage = req.CoverImage
	} else {
		return fiber.ErrBadRequest.Code, fiber.Map{
			"message":    "This field not found",
			"status":     "Bad request",
			"statusCode": fiber.ErrBadRequest.Code,
		}
	}

	updated, err := s.blogRepo.UpdateBlog(blogId, getBlog)
	if err != nil {
		logs.Error("Can't update blog", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"error": "Failed to update blog",
		}
	}

	return fiber.StatusOK, fiber.Map{
		"message":    "Blog successfully updated.",
		"status":     "OK",
		"statusCode": 200,
		"blogId":     updated.ID,
	}

}

func (s *blogService) BlogUpdateBID(blogId uint,blogReq *entities.BlogRequest) (int,fiber.Map) {
	getBlog, err := s.blogRepo.GetBlogBID(blogId)
	if err != nil {
		logs.Error("Can't get blog", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"error": "Failed to get blog",
		}
	}
	blog := entities.Blog{
		Model: gorm.Model{
			ID: blogId,
		},
		Title: blogReq.Title,
		BlogDesc: blogReq.BlogDesc,
		Content: blogReq.Content,
		CoverImage: blogReq.CoverImage,
		UserId: getBlog.UserId,
		UserName: getBlog.UserName,
		UserDesc: getBlog.UserDesc,
		UserImage: getBlog.UserImage,
	}

	updated, err := s.blogRepo.UpdateBlog(blogId,&blog)
	if err != nil {
		logs.Error("Can't update blog", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"error": "Failed to update blog",
		}
	}

	return fiber.StatusOK, fiber.Map{
		"message":    "Blog successfully updated.",
		"status":     "OK",
		"statusCode": 200,
		"data": updated,
	}
}
