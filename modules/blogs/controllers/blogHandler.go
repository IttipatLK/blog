package controllers

import (
	"blogservice/modules/entities"
	"blogservice/modules/logs"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"go.uber.org/zap"
)

type blogHandler struct {
	blogService entities.BlogService
}

func NewBlogHandler(r fiber.Router, blogService entities.BlogService) {

	controller := &blogHandler{blogService: blogService}

	r.Post("/createBlog/:userId",authMiddleware(controller), controller.CreateBlog)
	r.Get("/getBlogs", controller.GetBlogs)
	r.Get("/getBlog/:userId",authMiddleware(controller) , controller.GetBlog)
	r.Get("/getBlogId/:blogId",authMiddleware(controller),controller.GetBlogBID)
	r.Delete("/deleteBlog/:blogId",authMiddleware(controller),controller.DeleteBlog)
	r.Put("/updateBlog/:blogId",authMiddleware(controller),func(c *fiber.Ctx) error {
		textfield := c.Query("field")

		req := entities.UpdateOption{
			Field: textfield,
		}
		return controller.UpdateBlog(c,req)
	})
	r.Put("/updateBlogBID/:blogId",authMiddleware(controller),controller.UpdateBlogBID)
}

func authMiddleware(h *blogHandler) fiber.Handler {
	return func(c *fiber.Ctx) error {
		authHeader := c.Get("Authorization")
		if authHeader == "" {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"message": "Unauthorized: Missing Bearer Token",
			})
		}

		token := extractBearerToken(authHeader)
		if token == "" {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"message": "Unauthorized: Invalid Bearer Token",
			})
		}

		validToken, err := validateToken(token)
		if err != nil {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"message": fmt.Sprintf("Unauthorized: %s", err.Error()),
			})
		}

		// Check if the token is blacklisted
		checkBlackList, err := h.blogService.CheckBlackList(token)
		if err != nil {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"message": fmt.Sprintf("Can't check the blacklist token: %s", err.Error()),
			})
		}
		if checkBlackList > 0 {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"message": "The token has been revoked.",
			})
		}

		// Check if the token has expired
		if !isTokenExpired(validToken) {
			return c.Next()
		}

		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "The token has expired.",
		})
	}
}
// func authMiddleware(c *fiber.Ctx) error {
// 	authHeader := c.Get("Authorization")
// 	if authHeader == "" {
// 		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
// 			"message": "Unauthorized: Missing Bearer Token",
// 		})
// 	}

// 	token := extractBearerToken(authHeader)
// 	if token == "" {
// 		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
// 			"message": "Unauthorized: Invalid Bearer Token",
// 		})
// 	}

// 	validToken, err := validateToken(token)
// 	if err != nil {
// 		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
// 			"message": fmt.Sprintf("Unauthorized: %s", err.Error()),
// 		})
// 	}

	
// 	checkBlackList, err := h.blogService.CheckBlackList(token)
// 	if checkBlackList > 0 {
// 		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
// 			"message": "The token has been revoked.",
// 		})
// 	}

// 	if !isTokenExpired(validToken) {
// 		return c.Next()
// 	}

// 	return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
// 		"message": "The token has expired.",
// 	})
// }

func extractBearerToken(authHeader string) string {
	if len(authHeader) > 7 && strings.ToUpper(authHeader[0:6]) == "BEARER" {
		return authHeader[7:]
	}
	return ""
}

func validateToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("JWT_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}

	if token.Valid {
		return token, nil
	} else {
		return nil, fmt.Errorf("invalid token")
	}
}

func isTokenExpired(token *jwt.Token) bool {
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return true
	}

	exp, ok := claims["exp"].(float64)
	if !ok {
		return true
	}

	expirationTime := time.Unix(int64(exp), 0)

	return time.Now().After(expirationTime)
}

func (h *blogHandler) CreateBlog(c *fiber.Ctx) error {
	//param
	userID := c.Params("userId")

	if userID == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "userId could't empty",
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}

	userIdInt, err := strconv.Atoi(userID)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "Can't convert string to int",
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}

	var blogRequestBody entities.BlogRequest
	if err := c.BodyParser(&blogRequestBody); err != nil {
		logs.Info("Invalid request to Create Blog :", zap.Error(err))
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Invalid request",
		})
	}

	codeStatus, response := h.blogService.BlogCreated(userIdInt, &blogRequestBody)
	return c.Status(codeStatus).JSON(response)
}

func (h *blogHandler) GetBlogs(c *fiber.Ctx) error {
	response, err := h.blogService.BlogGets()
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "Can't get blogs",
			"dev":        err,
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}
	return c.JSON(fiber.Map{
		"message": "blogs retrieved successfully",
		"status":  fiber.StatusOK,
		"data":    response,
	})
}

func (h *blogHandler) GetBlog(c *fiber.Ctx) error {
	//param
	userID := c.Params("userId")
	if userID == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "userId could't empty",
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}
	IdUser, err := strconv.ParseUint(userID, 10, 64)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"Message":    "Invalid parameter type",
			"Status":     fiber.ErrBadRequest.Message,
			"StatusCode": fiber.ErrBadRequest.Code,
		})
	}

	response, err := h.blogService.BlogGetUID(uint(IdUser))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "Can't get blog with id",
			"dev":        err,
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}
	return c.JSON(fiber.Map{
		"message": "blog retrieved successfully",
		"status":  fiber.StatusOK,
		"data":    response,
	})

}

func (h *blogHandler) GetBlogBID(c *fiber.Ctx) error {
	//param
	blogID := c.Params("blogId")
	if blogID == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "blogId could't empty",
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}
	IdBlog, err := strconv.ParseUint(blogID, 10, 64)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"Message":    "Invalid parameter type",
			"Status":     fiber.ErrBadRequest.Message,
			"StatusCode": fiber.ErrBadRequest.Code,
		})
	}

	codeStatus, response := h.blogService.BlogGetBID(uint(IdBlog))
	return c.Status(codeStatus).JSON(response)
}



func (h *blogHandler) DeleteBlog(c *fiber.Ctx) error {
	//param
	blogID := c.Params("blogId")
	if blogID == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "blogId could't empty",
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}
	IdBlog, err := strconv.ParseUint(blogID, 10, 64)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"Message":    "Invalid parameter type",
			"Status":     fiber.ErrBadRequest.Message,
			"StatusCode": fiber.ErrBadRequest.Code,
		})
	}
	
	codeStatus, response := h.blogService.BlogDelete(uint(IdBlog))
	return c.Status(codeStatus).JSON(response)
}

func (h *blogHandler) UpdateBlog(c *fiber.Ctx, option entities.UpdateOption) error{
	//param
	blogID := c.Params("blogId")
	if blogID == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "blogId could't empty",
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}
	IdBlog, err := strconv.ParseUint(blogID, 10, 64)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"Message":    "Invalid parameter type",
			"Status":     fiber.ErrBadRequest.Message,
			"StatusCode": fiber.ErrBadRequest.Code,
		})
	}

	BlogUpdateBody := entities.BlogRequest{}
	if err := c.BodyParser(&BlogUpdateBody); err != nil {
		logs.Info("Invalid request to Update Blog", zap.Error(err))
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Invalid request",
		})
	}

	codeStatus, response := h.blogService.BlogUpdate(uint(IdBlog), BlogUpdateBody, option)

	return c.Status(codeStatus).JSON(response)
}

func (h *blogHandler) UpdateBlogBID(c *fiber.Ctx) error {
	//param
	blogID := c.Params("blogId")
	if blogID == "" {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message":    "blogId could't empty",
			"status":     fiber.ErrBadRequest.Message,
			"statusCode": fiber.ErrBadRequest.Code,
		})
	}
	IdBlog, err := strconv.ParseUint(blogID, 10, 64)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"Message":    "Invalid parameter type",
			"Status":     fiber.ErrBadRequest.Message,
			"StatusCode": fiber.ErrBadRequest.Code,
		})
	}

	BlogUpdateBody := entities.BlogRequest{}
	if err := c.BodyParser(&BlogUpdateBody); err != nil {
		logs.Info("Invalid request to Update Blog", zap.Error(err))
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Invalid request",
		})
	}

	codeStatus, response := h.blogService.BlogUpdateBID(uint(IdBlog),&BlogUpdateBody)
	return c.Status(codeStatus).JSON(response)
}
