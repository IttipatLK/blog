package entities

import "time"

var Topics = []string{
	UserCreated{}.TopicName(),
	TokenBlackListCreated{}.TopicName(),
}

type Event interface {
	TopicName() string
}

type EventHandler interface {
	Handle(topic string, evenBytes []byte)
}

type EventProducer interface {
	Produce(event Event) error
}

type UserCreated struct {
	UserID      int    `json:"userId"`
	Name        string `json:"name"`
	Description string `json:"description"`
	UserImage   string `json:"userImage"`
}

type TokenBlackListCreated struct {
	Token     string    `json:"token"`
	ExpiresAt time.Time `json:"expiresAt"`
}

// consume
func (UserCreated) TopicName() string {
	return "myblogs.user.created"
}

func (TokenBlackListCreated) TopicName() string {
	return "myblogs.user.tokenblacklist"
}

//option 
type UpdateOption struct {
	Field string `json:"field"`
}

