package entities

import (
	"github.com/gofiber/fiber/v2"
)

type BlogRepository interface {
	CreateBlog(blog *Blog) (*Blog, error)
	GetBlogs() ([]Blog, error)
	GetBlogUID(userId uint) (*[]Blog, error)
	GetBlogBID(blogId uint) (*Blog, error)
	UpdateBlog(blogId uint, blog *Blog) (*Blog, error)
	DeleteBlog(blogId uint) (uint, error)

	BlackListToken(tokenblacklist *TokenBlacklist) (*TokenBlacklist, error)
	CheckBlackList(token string) (int, error)

	CreateUser(user *User) (*User, error)
	CheckUser(userId int) (uint, error)
	GetUser(userId uint) (*User, error)
}

type BlogService interface {
	BlogCreated(userId int, blogReq *BlogRequest) (int, fiber.Map)
	BlogGets() (*[]BlogRes, error)
	BlogGetUID(userId uint) (*[]BlogRes, error)
	BlogGetBID(blogId uint) (int, fiber.Map)
	CheckBlackList(token string) (int, error)
	BlogDelete(blogId uint) (int, fiber.Map)
	BlogUpdate(blogId uint,req BlogRequest,option UpdateOption) (int,fiber.Map)
	BlogUpdateBID(blogId uint,blogReq *BlogRequest) (int,fiber.Map)
}
